package incrementDecrement;

public class Main {

    private static boolean f1;
    private static int f2 = 1;
    private static int balance = 10;

    public static void main(String[] args) {

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    if(!f1){
                        if(balance == 10){
                            if(f2 == 1){
                                System.out.println(balance);
                                f2--;
                            }
                            balance -= 10;
                        }
                    }
                    f1 = true;
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){

                    if(f1){
                        if(balance < 10){
                            if(f2 == 0){
                                System.out.println(balance);
                                f2++;
                            }
                            balance += 10;
                        }

                    }
                    f1 = false;
                }
            }
        });

        t1.start();
        t2.start();
    }
}
