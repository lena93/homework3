package scheduler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static int min;
    public static int sum = 0;

    static List<Integer> totalTimeList = new ArrayList<>();

    public static void main(String[] args) {

        Threadd t1 = new Threadd("Thread1", 100);
        Threadd t2 = new Threadd("Thread2", 200);
        Threadd t3 = new Threadd("Thread3", 150);
        Threadd t4 = new Threadd("Thread4", 220);
        Threadd t5 = new Threadd("Thread5", 170);

        List<Threadd> thr = new ArrayList<>();
        thr.add(t1);
        thr.add(t2);
        thr.add(t3);
        thr.add(t4);
        thr.add(t5);

        for (int i = 0; i < 5; i++) {

            for (Threadd t : thr) {
                if(t.totalTime == 0){
                    continue;
                }
                totalTimeList.add(t.totalTime);
            }

            System.out.println(totalTimeList.toString());
            min = Collections.min(totalTimeList);
            System.out.println("min = " + min);

            for (int j = 0; j < thr.size(); j++) {
                if(i == 0){
                    sum += min;
                    thr.get(j).startTime = sum;
                }

                thr.get(j).executionTime = min;
                if (thr.get(j).totalTime == 0) {
                    continue;
                }

                thr.get(j).totalTime -= thr.get(j).executionTime;
                System.out.println("executionTime - " + j + "-----" + thr.get(j).executionTime);
                System.out.println("totalTime - " + j + "-----" + thr.get(j).totalTime);
                System.out.println("startTime - " + j + "-----" + thr.get(j).startTime);

            }

            totalTimeList.clear();

        }
    }
}

class Threadd{
    public String name;
    public int totalTime;
    public int executionTime = 0;
    public int startTime;

    public Threadd(String name, int totalTime) {
        this.name = name;
        this.totalTime = totalTime;
    }

}
